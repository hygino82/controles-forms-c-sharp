﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Parte004
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            /* lista.Items.Add("Dilma");
             lista.Items.Add("Godofredo");
             lista.Items.Add("Gorete");
             lista.Items.Add("Jupira");
             lista.Items.Add("Juvenal");*/
            string nome = txtNome.Text;
            lista.Items.Add(nome);
            txtNome.Clear();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            lista.Items.Clear();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var valor = lista.SelectedItem;
            lista.Items.Remove(valor);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int posicao = lista.SelectedIndex;
            lista.Items.RemoveAt(posicao);
        }

        private void lista_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblResultado.Text = lista.SelectedItem.ToString();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            lblResultado.Text = lista.Items.Count.ToString();
        }
    }
}
