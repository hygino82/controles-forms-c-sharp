﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Rebar;

namespace Parte007
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            combo.Items.Add("Torresmo");
            combo.Items.Add("Banha");
            combo.Items.Add("Salame");
            combo.Items.Add("Repolho");

            combo.Items.AddRange(new object[] { "Sabão", "Farinha", "Prato" });
            //combo.Sorted = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Total de items " + combo.Items.Count);

            /*var item = combo.SelectedItem;
            int indice = combo.SelectedIndex;
            MessageBox.Show(indice + " - " + item);*/

            //combo.Items.Remove("Banha");

            //combo.Items.RemoveAt(3);

            //combo.Items.Insert(2, "Banana");

            MessageBox.Show(combo.Items.IndexOf("Salame").ToString());

        }

        private void combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            var item = combo.SelectedItem;
            int indice = combo.SelectedIndex;
            lblResultado.Text = indice + " - " + item.ToString().ToUpper();
        }
    }
}
