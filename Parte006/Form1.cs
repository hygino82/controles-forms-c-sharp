﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Parte006
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            checkList.Items.Add("Salame", true);
            checkList.Items.Add("Torresmo", false);
            checkList.Items.Add("Polenta", true);

            int qtd = checkList.Items.Count;
            // MessageBox.Show("Quantidade de items " + qtd);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //checkList.Items.Clear();
            // checkList.Items.Add("Pipoca");
            // checkList.Items.AddRange(new object[] { "Queijo", "Mortadela" });

            //var possui = checkList.Items.Contains("Salame");
            //MessageBox.Show(possui.ToString());
            //checkList.Items.Remove("Salame");
            //checkList.Items.RemoveAt(2);
            // checkList.Items.Insert(1, "Salsicha");//insere em uma posição informada

            //var checados = checkList.CheckedItems;//lista de items checados
            var checados = checkList.SelectedItems;//lista de items checados


            foreach (var item in checados)
            {
                MessageBox.Show(item.ToString());
            }
        }

        private void checkList_SelectedIndexChanged(object sender, EventArgs e)
        {
            label1.Text = checkList.SelectedItem.ToString();
        }
    }
}
