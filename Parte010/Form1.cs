﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Parte010
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            ListViewItem item1 = new ListViewItem("Dilma");
            item1.SubItems.Add("1214593");
            item1.SubItems.Add("dilma@Opressora.net");
            lista.Items.Add(item1);


            ListViewItem item2 = new ListViewItem("Gorete");
            item2.SubItems.Add("45112795");
            item2.SubItems.Add("gorete@gmail.com");
            lista.Items.Add(item2);

            ListViewItem item3 = new ListViewItem(new String[] { "Teodoro", "1477-2145", "Teodoro@gmail.com" });
            lista.Items.Add(item3);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //lista.Items.Clear();
            //     lista.Items.Remove(lista.SelectedItems[0]);
           // lista.Items.RemoveAt(1);
        }

        private void lista_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            MessageBox.Show(lista.SelectedItems[0].Text);
        }
    }
}
